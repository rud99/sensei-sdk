<?php

namespace Rud99\SenseiSdk;

use Illuminate\Support\Collection;
use Rud99\SenseiSdk\DataProvider\DataProviderManager;

class Product
{
    private $dataProvider;

    public function __construct()
    {
        $this->dataProvider = components(DataProviderManager::class);
    }

    public function getProducts($productType = false)
    {
        $products = $this->dataProvider->getData();
        if ($productType) $res = $this->filterProducts($products, $productType);
            else $res = $products;

        return $res;
    }

    public function getPopularProducts(Collection $products)
    {
        return $this->filterPopularProducts($products);
    }


    public function filterProducts(Collection $products, $type) : Collection
    {
        $res = $products
            ->filter(function($item) use ($type) {
                return $item['type'] == $type;
            })
            ->sortBy('price')
            ->values();

        return $res;
    }

    public function filterPopularProducts(Collection $products)
    {
        $res = $products
            ->filter(function($item) {
                return !empty($item['tags']);
            })
            ->filter(function($item) {
                return in_array('popular', $item['tags']);
            })
            ->shuffle()
            ->values()
            ->all();

        return $res;
    }
//
//    public function getGiftsProducts(Collection $products, array $giftsIds)
//    {
//        $res = $products
//            ->filter(function($item) use ($giftsIds) {
//                return in_array($item['id'], $giftsIds);
//            })
//            ->values()
//            ->all();
//
//        return $res;
//    }
}