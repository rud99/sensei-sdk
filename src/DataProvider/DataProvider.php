<?php
namespace Rud99\SenseiSdk\DataProvider;

abstract class DataProvider
{
    protected $dataService;

    public function __construct()
    {
        $this->dataService = 111;
    }

    abstract public function getData();
}
