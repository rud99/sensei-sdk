<?php
namespace Rud99\SenseiSdk\DataProvider;

class JsonDataProvider extends DataProvider
{
    private $file;

    public function __construct()
    {
        $this->file = config("data_file");
    }

    public function getData()
    {
        $string = file_get_contents($this->file);
        if (!$string) {
            echo __CLASS__ . ' - Не удалось зачитать файл ' . $jsonFile;
            die;
        }
        $data = json_decode($string, true);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $data_error = '';
                break;
            case JSON_ERROR_DEPTH:
                $data_error = 'Достигнута максимальная глубина стека';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $data_error = 'Неверный или не корректный JSON';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $data_error = 'Ошибка управляющего символа, возможно верная кодировка';
                break;
            case JSON_ERROR_SYNTAX:
                $data_error = 'Синтаксическая ошибка';
                break;
            case JSON_ERROR_UTF8:
                $data_error = 'Некорректные символы UTF-8, возможно неверная кодировка';
                break;
            default:
                $data_error = 'Неизвестная ошибка';
                break;
        }

        if ($data_error != '') {
            echo __CLASS__ . ' - ' . $data_error;
            die;
        }

        return collect($data['items']);
    }
}
