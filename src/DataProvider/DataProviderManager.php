<?php
namespace Rud99\SenseiSdk\DataProvider;

class DataProviderManager
{
    const DATA_JSON_FILE = 1;
    const DATA_DATABASE = 2;
    const DATA_REST = 3;


    private  $mode = self::DATA_JSON_FILE;

    public function __construct($mode)
    {
        $this->mode = $mode;
    }

    public function getInstance()
    {
        switch ($this->mode) {
            case (self::DATA_JSON_FILE):
                return new JsonDataProvider();
            case (self::DATA_DATABASE):
                return "DATA_DATABASE";
            case (self::DATA_REST):
                return "DATA_REST";
        }
    }
}